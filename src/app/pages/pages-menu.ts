import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'DASHBOARD',
    icon: 'fa fa-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
  {
    title: 'STUDENTS',
    icon: 'fa fa-user-o',
    link: '/pages/ui-features',
    children: [
      {
        title: 'Preinscriptions',
        link: '/pages/ui-features/buttons',
        icon: 'fa fa-pencil-square-o',
      },
      {
        title: 'Inscriptions',
        link: '/pages/ui-features/grid',
        icon: 'fa fa-pencil',
      },
      {
        title: 'Informations',
        link: '/pages/ui-features/icons',
        icon: 'fa  fa-info',
      },
      {
        title: 'Promotion',
        link: '/pages/ui-features/modals',
        icon: 'fa fa-graduation-cap',
      },
      /* A mettre en commentaire*/
      {
        title: 'Typography',
        link: '/pages/ui-features/typography',
      },
      {
        title: 'Animated Searches',
        link: '/pages/ui-features/search-fields',
      },
      {
        title: 'Tabs',
        link: '/pages/ui-features/tabs',
      },
    ],
  },
  {
    title: 'TEACHERS',
    icon: 'fa fa-user-circle-o',
    children: [
      {
        title: 'Teachers management',
        icon: 'fa fa-user-plus',
        link: '/pages/forms/inputs',
      },
      {
        title: 'Teaching management',
        icon: 'fa fa-file-text',
        link: '/pages/forms/layouts',
      }, /* FIN commentaires*/ 
    ],
  },
  {
    title: 'PARENTS',
    icon: 'fa fa-user-secret',
    children: [
      {
        title: 'Parents management',
        icon: 'fa fa-user-secret',
        link: '/pages/components/tree',
      }, {
        title: 'Parents contact',
        icon: 'fa fa-address-book-o',
        link: '/pages/components/notifications',
      },
    ],
  },
  {
    title: 'LIBRARY',
    icon: 'fa fa-book',
    children: [
      {
        title: 'Books management',
        icon: 'fa fa-files-o',
        link: '/pages/maps/gmaps',
      },
      {
        title: 'Borrowings',
        icon: 'fa fa-sign-out',
        link: '/pages/maps/leaflet',
      },
      {
        title: 'Readers management',
        icon: 'fa fa-users',
        link: '/pages/maps/bubble',
      },
    ],
  },
  {
    title: 'ACCOUNTING',
    icon: 'fa fa-money',
    children: [
      {
        title: 'Student payment',
        icon: 'fa fa-credit-card',
        link: '/pages/charts/echarts',
      },
      {
        title: 'Accounting',
        icon: 'fa fa-line-chart',
        link: '/pages/charts/chartjs',
      },
      {
        title: 'D3',
        link: '/pages/charts/d3',
      },
    ],
  },
  {
    title: 'CLASS',
    icon: 'nb-title',
    children: [
      {
        title: 'Manage levels',
        icon: 'fa fa-street-view',
        link: '/pages/editors/tinymce',
      },
      {
        title: 'Manage classes',
        icon: 'fa fa-group',
        link: '/pages/editors/ckeditor',
      },
    ],
  },
  {
    title: 'SUBJECT',
    icon: 'nb-tables',
    children: [
      {
        title: 'Smart Table',
        link: '/pages/tables/smart-table',
      },
    ],
  },
  {
    title: 'EXAM',
    icon: 'nb-locked',
    children: [
      {
        title: 'Manage exam',
        link: '/auth/login',
      },
      {
        title: 'Manage marks',
        link: '/auth/register',
      },
      {
        title: 'Marks reports',
        link: '/auth/request-password',
      },
      {
        title: 'Exam questions',
        link: '/auth/reset-password',
      },
    ],
  }, 
];
